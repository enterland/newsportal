﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IndiaMoves.Models;

namespace IndiaMoves
{
    public class Newss
    {
        public int NewsId { get; set; }
        public string Path { get; set; }
        public string NewsHeadline { get; set; }
        public DateTime UploadDate { get; set; }

        public string UploadUser { get; set; }
        public string Description { get; set; }

    }

    public partial class Default : System.Web.UI.Page
    {
        IndiamovesDBEntities db = new IndiamovesDBEntities();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                getAllNewsImg();
                BindNewsDataList(0);
            }
        }

        private List<Newss> GetNewsEntity()
        {
            


            //var nws = from m in db.News
            //          select new Newss
            //          {
            //              NewsId =m.NewsId,
            //              Path = m.Path.Substring(1, m.Path.Length),
            //              NewsHeadline = m.NewsHeadline,
            //              UploadDate =(DateTime)m.UploadDate,
            //              UploadUser =m.UserMaster.UserID,
            //              Description = (m.Description.Substring(0, 100) + " ....")
            //          };

            //return nws.ToList();


            var b = db.NewsImages.Where(s => s.isCover == true).Select(m => new Newss
            {
                NewsId =(Int32)m.NewsID,
                Path = m.Path.Substring(1, m.Path.Length),
                NewsHeadline = m.News.NewsHeadline,
                UploadDate = (DateTime)m.News.UploadDate,
                UploadUser = m.News.UserMaster.UserID,
                Description = (m.News.Description.Substring(0, 100) + " ....")
            }).OrderByDescending(k => k.NewsId).ToList();

            return b.ToList();

        }

        private void BindNewsDataList(int pageIndex)
        {

            int totalRecords = GetNewsEntity().Count;
            int pageSize = 10;
            int startRow = pageIndex * pageSize;

            dlNews.DataSource = GetNewsEntity().Skip(startRow).Take(pageSize);
            dlNews.DataBind();

            BindPager(totalRecords, pageIndex, pageSize);

        }

        private void BindPager(int totalRecordCount, int currentPageIndex, int pageSize)
        {
            double getPageCount = (double)((decimal)totalRecordCount / (decimal)pageSize);
            int pageCount = (int)Math.Ceiling(getPageCount);
            List<ListItem> pages = new List<ListItem>();
            if (pageCount > 1)
            {
                pages.Add(new ListItem("FIRST", "1", currentPageIndex > 1));
                for (int i = 1; i <= pageCount; i++)
                {
                    pages.Add(new ListItem(i.ToString(), i.ToString(), i != currentPageIndex + 1));
                }
                pages.Add(new ListItem("LAST", pageCount.ToString(), currentPageIndex < pageCount - 1));
            }

            rptPager.DataSource = pages;
            rptPager.DataBind();
        }

        protected void Page_Changed(object sender, EventArgs e)
        {
            int pageIndex = Convert.ToInt32(((sender as LinkButton).CommandArgument));
            BindNewsDataList(pageIndex - 1);
        }

        private void getAllNewsImg()
        {
            //all top 5 news DataList
            var a = db.NewsImages.Where(s => s.isCover == true).Select(m => new { newsId = m.NewsID, path = m.Path.Substring(1, m.Path.Length), name = m.News.NewsHeadline, UploadDate = m.News.UploadDate, UploadBy = m.News.UserMaster.UserID, ShortDesc = (m.News.Description.Substring(0, 100) + " ....") }).OrderByDescending(j => j.newsId).Take(5).ToList();
            dlTrending.DataSource = a;
            dlTrending.DataBind();

            //all Main 5 news DataList
            int[] ar = new int[5];
            DateTime todaym = DateTime.Now;
            DateTime last7m = todaym.AddDays(-7);
            var mn = db.NewsImages.Where(l => l.isCover == true && l.News.UploadDate >= last7m).OrderByDescending(k => k.News.NewsId).Take(5);

            int i = 0;
            foreach(NewsImage n in mn)
            {
                if (i > 5)
                    return;
                ar[i] = n.News.NewsId;
                i += 1;
            }

            int a1 = ar[0];
            int a2 = ar[1];
            int a3 = ar[2];
            int a4 = ar[3];
            int a5 = ar[4];


            var c = db.NewsImages.Where(s => s.isCover == true && s.News.NewsId==a1).Select(m => new { newsId = m.NewsID, path = m.Path.Substring(1, m.Path.Length), name = m.News.NewsHeadline, UploadDate = m.News.UploadDate, UploadBy = m.News.UserMaster.UserID, ShortDesc = (m.News.Description.Substring(0, 100) + " ....") }).ToList();
            dlMainNews1.DataSource = c;
            dlMainNews1.DataBind();
            var c2 = db.NewsImages.Where(s => s.isCover == true && s.News.NewsId ==a2 ).Select(m => new { newsId = m.NewsID, path = m.Path.Substring(1, m.Path.Length), name = m.News.NewsHeadline, UploadDate = m.News.UploadDate, UploadBy = m.News.UserMaster.UserID, ShortDesc = (m.News.Description.Substring(0, 100) + " ....") }).ToList();
            dlMainNews2.DataSource = c2;
            dlMainNews2.DataBind();
            var c3 = db.NewsImages.Where(s => s.isCover == true && s.News.NewsId == a3).Select(m => new { newsId = m.NewsID, path = m.Path.Substring(1, m.Path.Length), name = m.News.NewsHeadline, UploadDate = m.News.UploadDate, UploadBy = m.News.UserMaster.UserID, ShortDesc = (m.News.Description.Substring(0, 100) + " ....") }).ToList();
            dlMainNews3.DataSource = c3;
            dlMainNews3.DataBind();
            var c4 = db.NewsImages.Where(s => s.isCover == true && s.News.NewsId == a4).Select(m => new { newsId = m.NewsID, path = m.Path.Substring(1, m.Path.Length), name = m.News.NewsHeadline, UploadDate = m.News.UploadDate, UploadBy = m.News.UserMaster.UserID, ShortDesc = (m.News.Description.Substring(0, 100) + " ....") }).ToList();
            dlMainNews4.DataSource = c4;
            dlMainNews4.DataBind();
            var c5 = db.NewsImages.Where(s => s.isCover == true && s.News.NewsId == a5).Select(m => new { newsId = m.NewsID, path = m.Path.Substring(1, m.Path.Length), name = m.News.NewsHeadline, UploadDate = m.News.UploadDate, UploadBy = m.News.UserMaster.UserID, ShortDesc = (m.News.Description.Substring(0, 100) + " ....") }).ToList();
            dlMainNews5.DataSource = c5;
            dlMainNews5.DataBind();

            //all news Recent Post

            DateTime today = DateTime.Now;
            DateTime last10 = today.AddDays(-10);
            var b = db.News.Where(s => s.UploadDate> last10).Select(m => new { newsId = m.NewsId, name = m.NewsHeadline,}).OrderByDescending(k => k.newsId).ToList();
            dlRecentPost.DataSource = b;
            dlRecentPost.DataBind();


           

            ////all news DataList
            //var b = db.NewsImages.Where(s => s.isCover == true ).Select(m => new { newsId = m.NewsID, path = m.Path.Substring(1, m.Path.Length), name = m.News.NewsHeadline, UploadDate=m.News.UploadDate, UploadBy=m.News.UserMaster.UserID, ShortDesc=(m.News.Description.Substring(0,100)+" ....") }).Take(5).ToList();
            //dlNews.DataSource = b;
            //dlNews.DataBind();


            //PagedDataSource pg = new PagedDataSource();
            //pg.DataSource = b;
            //pg.AllowPaging = true;
            //pg.PageSize = 10;

            ////Binding pg to datalist
            //dlNews.DataSource = pg;//dl is datalist
            //dlNews.DataBind();

        }
    }
}