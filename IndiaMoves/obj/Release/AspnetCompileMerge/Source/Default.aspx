﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterStatic.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="IndiaMoves.Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section
        class="aft-blocks aft-main-banner-section banner-carousel-1-wrap bg-fixed default af-main-banner-boxed"
        dir="ltr">


        <!-- <div class="banner-carousel-1 af-widget-carousel owl-carousel owl-theme"> -->
        <div class="aft-main-banner-wrapper clearfix aft-add-gaps-between">
            <div class="aft-banner-box-wrapper af-container-row clearfix aft-main-banner-trending-left">


                <div class="af-trending-news-part float-l col-30 pad left ">


                    <div class="em-title-subtitle-wrap">
                        <h4 class="widget-title header-after1">
                            <span class="header-after category-color-1">Trending                        </span>
                        </h4>
                    </div>

                    <div class="af-main-banner-trending-posts trending-posts" dir="ltr">
                        <div class="section-wrapper">
                            <div class="af-double-column list-style clearfix">

                                <asp:DataList ID="dlTrending" data-mh="af-feat-list" class="col-1" runat="server">
                                    <ItemTemplate>
                                        <div class="read-single color-pad">
                                            <div class="data-bg read-img pos-rel col-4 float-l read-bg-img"
                                                data-background='<%# Eval("path") %>'>
                                                <img src="<%# Eval("path") %>" />
                                                <%--<a href="2020/08/23/ranchi-the-word-kudmi-is-only-in-kolhan-add-the-word-kurmi-used-lalchand-mahato-jharkhand/Default.aspx"></a>--%>
                                                <a href="/ViewNews.aspx?id=<%#Eval("NewsId")%>" class=""></a>
                                                <div class="trending-post-items pos-rel col-4 float-l show-inside-image">
                                                    <span class="trending-no">
                                                        <asp:Label ID="labTrendNo" runat="server"></asp:Label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="trending-post-items pos-rel col-4 float-l">
                                            </div>
                                            <div class="read-details col-75 float-l pad color-tp-pad">
                                                <div class="read-categories">
                                                    <ul class="cat-links">
                                                        <li class="meta-category">
                                                            <a class="newsever-categories category-color-1" href="#" alt="View all posts in NEWS">NEWS
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="read-title">
                                                    <h4>
                                                        
                                                        <a href="/ViewNews.aspx?id=<%#Eval("NewsId")%>" class="">
                                                            <asp:Label ID="labHeadLine" runat="server" Text='<%#Eval("name")%>' /></a>
                                                    </h4>
                                                </div>

                                                <div class="entry-meta">

                                                    <span class="author-links">

                                                        <span class="item-metadata posts-date">
                                                            <i class="fa fa-clock-o"></i>
                                                            <%--August 23, 2020--%><asp:Label ID="LabUploadDate" runat="server" Text='<%#Eval("UploadDate")%>' />
                                                        </span>

                                                        <span class="item-metadata posts-author byline">
                                                            <i class="fa fa-pencil-square-o"></i>
                                                            <a href="#"><%--admin--%>
                                                                <asp:Label ID="LabUploadBy" runat="server" Text='<%#Eval("UploadBy")%>' />
                                                            </a>
                                                        </span>

                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </div>
                    </div>

                    <!-- Trending line END -->
                </div>


                <div class="aft-carousel-part float-l col-70 pad">



                    <div class="em-title-subtitle-wrap">
                        <h4 class="widget-title header-after1">
                            <span class="header-after category-color-1">Main News                        </span>
                        </h4>
                    </div>

                    <div class="af-banner-carousel-1 af-widget-carousel slick-wrapper banner-carousel-slider title-under-image"
                        data-slick='{"slidesToShow":1,"autoplaySpeed":8000,"slidesToScroll":1,"centerMode":false,"responsive":[{"breakpoint":1024,"settings":{"slidesToShow":1,"slidesToScroll":1,"infinite":true}},{"breakpoint":769,"settings":{"slidesToShow":1,"slidesToScroll":1,"infinite":true}},{"breakpoint":480,"settings":{"slidesToShow":1,"slidesToScroll":1,"infinite":true}}]}'>
                        <div class="slick-item">
                            <asp:DataList ID="dlMainNews1" class="read-single color-pad pos-rel" runat="server">
                                <ItemTemplate>
                                    <%--<div class="read-single color-pad pos-rel">--%>
                                    <div class="read-img pos-rel read-img read-bg-img data-bg"
                                        data-background='<%# Eval("path") %>'>
                                        <%--<a class="aft-slide-items" href="2020/08/23/ranchi-the-word-kudmi-is-only-in-kolhan-add-the-word-kurmi-used-lalchand-mahato-jharkhand/Default.aspx"></a>--%>
                                        <a class="aft-slide-items" href="/ViewNews.aspx?id=<%#Eval("NewsId")%>"></a>
                                        <%--<img src="wp-content/uploads/2020/08/20200823_174920.jpg">--%>
                                        <img src="<%# Eval("path") %>" />

                                        <div class="min-read-post-format">
                                            <span class="min-read-item">
                                                <span class="min-read">5 min read</span>                            </span>
                                        </div>
                                    </div>
                                    <div class="read-details color-tp-pad">
                                        <div class="read-categories ">
                                            <ul class="cat-links">
                                                <li class="meta-category">
                                                    <a class="newsever-categories category-color-1" href="#" alt="View all posts in NEWS">NEWS
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="read-title">
                                            <h4>
                                                
                                                <a href="/ViewNews.aspx?id=<%#Eval("NewsId")%>" class="link-product-add-cart">
                                                    <asp:Label ID="labHeadLine" runat="server" Text='<%#Eval("name")%>' /></a>
                                            </h4>
                                        </div>
                                        <div class="entry-meta">

                                            <span class="author-links">

                                                <span class="item-metadata posts-date">
                                                    <i class="fa fa-clock-o"></i>
                                                    <%--August 23, 2020--%><asp:Label ID="LabUploadDate" runat="server" Text='<%#Eval("UploadDate")%>' />
                                                </span>

                                                <span class="item-metadata posts-author byline">
                                                    <i class="fa fa-pencil-square-o"></i>
                                                    <a href="author/indiamoves/Default.aspx"><%--admin--%>
                                                        <asp:Label ID="LabUploadBy" runat="server" Text='<%#Eval("UploadBy")%>' />
                                                    </a>
                                                </span>

                                            </span>
                                        </div>

                                    </div>
                                    <%--</div>--%>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                        <div class="slick-item">
                            <asp:DataList ID="dlMainNews2" class="read-single color-pad pos-rel" runat="server">
                                <ItemTemplate>
                                    <%--<div class="read-single color-pad pos-rel">--%>
                                    <div class="read-img pos-rel read-img read-bg-img data-bg"
                                        data-background='<%# Eval("path") %>'>
                                        <%--<a class="aft-slide-items" href="2020/08/23/ranchi-the-word-kudmi-is-only-in-kolhan-add-the-word-kurmi-used-lalchand-mahato-jharkhand/Default.aspx"></a>--%>
                                        <a class="aft-slide-items" href="/ViewNews.aspx?id=<%#Eval("NewsId")%>"></a>
                                        <%--<img src="wp-content/uploads/2020/08/20200823_174920.jpg">--%>
                                        <img src="<%# Eval("path") %>" />

                                        <div class="min-read-post-format">
                                            <span class="min-read-item">
                                                <span class="min-read">5 min read</span>                            </span>
                                        </div>
                                    </div>
                                    <div class="read-details color-tp-pad">
                                        <div class="read-categories ">
                                            <ul class="cat-links">
                                                <li class="meta-category">
                                                    <a class="newsever-categories category-color-1" href="#" alt="View all posts in NEWS">NEWS
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="read-title">
                                            <h4>
                                                
                                                <a href="/ViewNews.aspx?id=<%#Eval("NewsId")%>" class="link-product-add-cart">
                                                    <asp:Label ID="labHeadLine" runat="server" Text='<%#Eval("name")%>' /></a>
                                            </h4>
                                        </div>
                                        <div class="entry-meta">

                                            <span class="author-links">

                                                <span class="item-metadata posts-date">
                                                    <i class="fa fa-clock-o"></i>
                                                    <%--August 23, 2020--%><asp:Label ID="LabUploadDate" runat="server" Text='<%#Eval("UploadDate")%>' />
                                                </span>

                                                <span class="item-metadata posts-author byline">
                                                    <i class="fa fa-pencil-square-o"></i>
                                                    <a href="author/indiamoves/Default.aspx"><%--admin--%>
                                                        <asp:Label ID="LabUploadBy" runat="server" Text='<%#Eval("UploadBy")%>' />
                                                    </a>
                                                </span>

                                            </span>
                                        </div>

                                    </div>
                                    <%--</div>--%>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                        <div class="slick-item">
                            <asp:DataList ID="dlMainNews3" class="read-single color-pad pos-rel" runat="server">
                                <ItemTemplate>
                                    <%--<div class="read-single color-pad pos-rel">--%>
                                    <div class="read-img pos-rel read-img read-bg-img data-bg"
                                        data-background='<%# Eval("path") %>'>
                                        <%--<a class="aft-slide-items" href="2020/08/23/ranchi-the-word-kudmi-is-only-in-kolhan-add-the-word-kurmi-used-lalchand-mahato-jharkhand/Default.aspx"></a>--%>
                                        <a class="aft-slide-items" href="/ViewNews.aspx?id=<%#Eval("NewsId")%>"></a>
                                        <%--<img src="wp-content/uploads/2020/08/20200823_174920.jpg">--%>
                                        <img src="<%# Eval("path") %>" />

                                        <div class="min-read-post-format">
                                            <span class="min-read-item">
                                                <span class="min-read">5 min read</span>                            </span>
                                        </div>
                                    </div>
                                    <div class="read-details color-tp-pad">
                                        <div class="read-categories ">
                                            <ul class="cat-links">
                                                <li class="meta-category">
                                                    <a class="newsever-categories category-color-1" href="#" alt="View all posts in NEWS">NEWS
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="read-title">
                                            <h4>
                                                
                                                <a href="/ViewNews.aspx?id=<%#Eval("NewsId")%>" class="link-product-add-cart">
                                                    <asp:Label ID="labHeadLine" runat="server" Text='<%#Eval("name")%>' /></a>
                                            </h4>
                                        </div>
                                        <div class="entry-meta">

                                            <span class="author-links">

                                                <span class="item-metadata posts-date">
                                                    <i class="fa fa-clock-o"></i>
                                                    <%--August 23, 2020--%><asp:Label ID="LabUploadDate" runat="server" Text='<%#Eval("UploadDate")%>' />
                                                </span>

                                                <span class="item-metadata posts-author byline">
                                                    <i class="fa fa-pencil-square-o"></i>
                                                    <a href="author/indiamoves/Default.aspx"><%--admin--%>
                                                        <asp:Label ID="LabUploadBy" runat="server" Text='<%#Eval("UploadBy")%>' />
                                                    </a>
                                                </span>

                                            </span>
                                        </div>

                                    </div>
                                    <%--</div>--%>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                        <div class="slick-item">
                            <asp:DataList ID="dlMainNews4" class="read-single color-pad pos-rel" runat="server">
                                <ItemTemplate>
                                    <%--<div class="read-single color-pad pos-rel">--%>
                                    <div class="read-img pos-rel read-img read-bg-img data-bg"
                                        data-background='<%# Eval("path") %>'>
                                        <%--<a class="aft-slide-items" href="2020/08/23/ranchi-the-word-kudmi-is-only-in-kolhan-add-the-word-kurmi-used-lalchand-mahato-jharkhand/Default.aspx"></a>--%>
                                        <a class="aft-slide-items" href="/ViewNews.aspx?id=<%#Eval("NewsId")%>"></a>
                                        <%--<img src="wp-content/uploads/2020/08/20200823_174920.jpg">--%>
                                        <img src="<%# Eval("path") %>" />

                                        <div class="min-read-post-format">
                                            <span class="min-read-item">
                                                <span class="min-read">5 min read</span>                            </span>
                                        </div>
                                    </div>
                                    <div class="read-details color-tp-pad">
                                        <div class="read-categories ">
                                            <ul class="cat-links">
                                                <li class="meta-category">
                                                    <a class="newsever-categories category-color-1" href="#" alt="View all posts in NEWS">NEWS
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="read-title">
                                            <h4>
                                                
                                                <a href="/ViewNews.aspx?id=<%#Eval("NewsId")%>" class="link-product-add-cart">
                                                    <asp:Label ID="labHeadLine" runat="server" Text='<%#Eval("name")%>' /></a>
                                            </h4>
                                        </div>
                                        <div class="entry-meta">

                                            <span class="author-links">

                                                <span class="item-metadata posts-date">
                                                    <i class="fa fa-clock-o"></i>
                                                    <%--August 23, 2020--%><asp:Label ID="LabUploadDate" runat="server" Text='<%#Eval("UploadDate")%>' />
                                                </span>

                                                <span class="item-metadata posts-author byline">
                                                    <i class="fa fa-pencil-square-o"></i>
                                                    <a href="author/indiamoves/Default.aspx"><%--admin--%>
                                                        <asp:Label ID="LabUploadBy" runat="server" Text='<%#Eval("UploadBy")%>' />
                                                    </a>
                                                </span>

                                            </span>
                                        </div>

                                    </div>
                                    <%--</div>--%>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                        <div class="slick-item">
                            <asp:DataList ID="dlMainNews5" class="read-single color-pad pos-rel" runat="server">
                                <ItemTemplate>
                                    <%--<div class="read-single color-pad pos-rel">--%>
                                    <div class="read-img pos-rel read-img read-bg-img data-bg"
                                        data-background='<%# Eval("path") %>'>
                                        <%--<a class="aft-slide-items" href="2020/08/23/ranchi-the-word-kudmi-is-only-in-kolhan-add-the-word-kurmi-used-lalchand-mahato-jharkhand/Default.aspx"></a>--%>
                                        <a class="aft-slide-items" href="/ViewNews.aspx?id=<%#Eval("NewsId")%>"></a>
                                        <%--<img src="wp-content/uploads/2020/08/20200823_174920.jpg">--%>
                                        <img src="<%# Eval("path") %>" />

                                        <div class="min-read-post-format">
                                            <span class="min-read-item">
                                                <span class="min-read">5 min read</span>                            </span>
                                        </div>
                                    </div>
                                    <div class="read-details color-tp-pad">
                                        <div class="read-categories ">
                                            <ul class="cat-links">
                                                <li class="meta-category">
                                                    <a class="newsever-categories category-color-1" href="#" alt="View all posts in NEWS">NEWS
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="read-title">
                                            <h4>
                                                
                                                <a href="/ViewNews.aspx?id=<%#Eval("NewsId")%>" class="link-product-add-cart">
                                                    <asp:Label ID="labHeadLine" runat="server" Text='<%#Eval("name")%>' /></a>
                                            </h4>
                                        </div>
                                        <div class="entry-meta">

                                            <span class="author-links">

                                                <span class="item-metadata posts-date">
                                                    <i class="fa fa-clock-o"></i>
                                                    <%--August 23, 2020--%><asp:Label ID="LabUploadDate" runat="server" Text='<%#Eval("UploadDate")%>' />
                                                </span>

                                                <span class="item-metadata posts-author byline">
                                                    <i class="fa fa-pencil-square-o"></i>
                                                    <a href="author/indiamoves/Default.aspx"><%--admin--%>
                                                        <asp:Label ID="LabUploadBy" runat="server" Text='<%#Eval("UploadBy")%>' />
                                                    </a>
                                                </span>

                                            </span>
                                        </div>

                                    </div>
                                    <%--</div>--%>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </div>
                </div>


                <%--                <div class="float-l af-editors-pick col-30 pad left ">


                    <div class="em-title-subtitle-wrap">
                        <h4 class="widget-title header-after1">
                            <span class="header-after category-color-1">Editorials                        </span>
                        </h4>
                    </div>

                    <div class="af-main-banner-featured-posts featured-posts" dir="ltr">

                        <div class="section-wrapper">
                            <div class="small-gird-style af-container-row clearfix">
                                <div class="float-l big-grid af-category-inside-img af-sec-post col-1 pad">
                                    <div class="read-single pos-rel">
                                        <div class="data-bg read-img pos-rel read-bg-img"
                                            data-background="wp-content/uploads/2020/08/20200823_174920.jpg">
                                            <img src="wp-content/uploads/2020/08/20200823_174920.jpg">
                                            <a class="aft-slide-items" href="2020/08/23/ranchi-the-word-kudmi-is-only-in-kolhan-add-the-word-kurmi-used-lalchand-mahato-jharkhand/Default.aspx"></a>

                                            <div class="min-read-post-format">
                                                <span class="min-read-item">
                                                    <span class="min-read">5 min read</span>                                            </span>
                                            </div>
                                        </div>
                                        <div class="read-details">

                                            <div class="read-categories af-category-inside-img">

                                                <ul class="cat-links">
                                                    <li class="meta-category">
                                                        <a class="newsever-categories category-color-1" href="category/media-2/news/Default.aspx" alt="View all posts in NEWS">NEWS
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="read-title">
                                                <h4>
                                                    
                                                </h4>
                                            </div>
                                            <div class="entry-meta">

                                                <span class="author-links">

                                                    <span class="item-metadata posts-date">
                                                        <i class="fa fa-clock-o"></i>
                                                        August 23, 2020            </span>

                                                    <span class="item-metadata posts-author byline">
                                                        <i class="fa fa-pencil-square-o"></i>
                                                        <a href="author/indiamoves/Default.aspx">admin                        </a>
                                                    </span>

                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="float-l big-grid af-category-inside-img af-sec-post col-1 pad">
                                    <div class="read-single pos-rel">
                                        <div class="data-bg read-img pos-rel read-bg-img"
                                            data-background="wp-content/uploads/2020/08/20200823_164904-720x475.jpg">
                                            <img src="wp-content/uploads/2020/08/20200823_164904-720x475.jpg">
                                            <a class="aft-slide-items" href="2020/08/23/jharkhand-on-the-occasion-of-the-birth-anniversary-of-former-vice-chancellor-dr-ram-dayal-munda-his-statue-was-garlanded-and-planted-a-tree/Default.aspx"></a>

                                            <div class="min-read-post-format">
                                                <span class="min-read-item">
                                                    <span class="min-read">5 min read</span>                                            </span>
                                            </div>
                                        </div>
                                        <div class="read-details">

                                            <div class="read-categories af-category-inside-img">

                                                <ul class="cat-links">
                                                    <li class="meta-category">
                                                        <a class="newsever-categories category-color-1" href="category/media-2/news/Default.aspx" alt="View all posts in NEWS">NEWS
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="read-title">
                                                <h4>
                                                    <a href="2020/08/23/jharkhand-on-the-occasion-of-the-birth-anniversary-of-former-vice-chancellor-dr-ram-dayal-munda-his-statue-was-garlanded-and-planted-a-tree/Default.aspx">Jharkhand -माण्डर विधायक बंधु तिर्की ने डॉ रामदयाल मुंडा जी की जयंती के अवसर पर उनकी प्रतिमा पर माल्यार्पण कर वृक्षारोपण किया,</a>
                                                </h4>
                                            </div>
                                            <div class="entry-meta">

                                                <span class="author-links">

                                                    <span class="item-metadata posts-date">
                                                        <i class="fa fa-clock-o"></i>
                                                        August 23, 2020            </span>

                                                    <span class="item-metadata posts-author byline">
                                                        <i class="fa fa-pencil-square-o"></i>
                                                        <a href="author/indiamoves/Default.aspx">admin                        </a>
                                                    </span>

                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- Editors Pick line END -->
                </div>
                --%>
            </div>
        </div>


    </section>

    <!-- end slider-section -->

    <div class="container-wrapper">
    </div>




    <div id="content" class="container-wrapper ">
        <section class="section-block-upper af-container-block-wrapper clearfix">

            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <!--<div class="af-container-row">-->

                    <div class="af-container-row aft-archive-wrapper clearfix archive-layout-list">
                        <!--<div class="af-container-row aft-archive-wrapper clearfix ">-->
                        <%--<article id="post-4246" class="latest-posts-list col-1 float-l pad archive-layout-list archive-image-left post-4246 post type-post status-publish format-standard has-post-thumbnail hentry category-news tag-corona tag-culture tag-media tag-news tag-sports">--%>
                        <asp:DataList ID="dlNews" class="latest-posts-list col-1 float-l pad archive-layout-list archive-image-left post-4246 post type-post status-publish format-standard has-post-thumbnail hentry category-news tag-corona tag-culture tag-media tag-news tag-sports" runat="server">
                            <ItemTemplate>
                                <div class="archive-list-post list-style">
                                    <div class="read-single color-pad" style="margin-bottom:20px;">

                                        <div class="data-bg read-img pos-rel col-2 float-l read-bg-img af-sec-list-img"
                                            data-background='<%# Eval("Path") %>'>
                                            <%--<img src="wp-content/uploads/2020/08/20200823_174920.jpg">--%>
                                            <img src="<%# Eval("Path") %>">
                                            <%--<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl='<%# Eval("path") %>' />--%>
                                            <div class="min-read-post-format">
                                                <span class="min-read-item">
                                                    <span class="min-read">5 min read</span>                            </span>
                                            </div>

                                            <%--<a href="2020/08/23/ranchi-the-word-kudmi-is-only-in-kolhan-add-the-word-kurmi-used-lalchand-mahato-jharkhand/Default.aspx"></a>--%>
                                            <a href="/ViewNews.aspx?id=<%#Eval("NewsId")%>" class="link-product-add-cart"></a>
                                        </div>


                                        <div class="read-details col-2 float-l pad af-sec-list-txt color-tp-pad">
                                            <div class="read-categories">
                                                <ul class="cat-links">
                                                    <li class="meta-category">
                                                        <a class="newsever-categories category-color-1" href="category/media-2/news/Default.aspx" alt="View all posts in NEWS">NEWS
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="read-title">
                                                <h4>

                                                   
                                                    <a href="/ViewNews.aspx?id=<%#Eval("NewsId")%>" class="link-product-add-cart">
                                                        <asp:Label ID="labHeadLine" runat="server" Text='<%#Eval("NewsHeadline")%>' /></a>
                                                </h4>
                                            </div>
                                            <div class="entry-meta">

                                                <span class="author-links">

                                                    <span class="item-metadata posts-date">
                                                        <i class="fa fa-clock-o"></i>
                                                        <%--August 23, 2020--%>
                                                        <asp:Label ID="LabUploadDate" runat="server" Text='<%#Eval("UploadDate")%>' />
                                                    </span>

                                                    <span class="item-metadata posts-author byline">
                                                        <i class="fa fa-pencil-square-o"></i>
                                                        <a href="#">
                                                            <asp:Label ID="LabUploadBy" runat="server" Text='<%#Eval("UploadUser")%>' />
                                                        </a>
                                                    </span>

                                                </span>
                                            </div>

                                            <div class="read-descprition full-item-discription">
                                                <div class="post-description">
                                                    <p>
                                                        <asp:Label ID="labShortDesc" runat="server" Text='<%#Eval("Description")%>' />
                                                    </p>
                                                    <%--<p>&#2360;&#2368;&#2319;&#2350; &#2325;&#2375; &#2346;&#2361;&#2354; &#2325;&#2366; &#2323;&#2348;&#2368;&#2360;&#2368; &#2344;&#2375;&#2340;&#2366;&#2323;&#2306; &#2344;&#2375; &#2325;&#2367;&#2351;&#2366; &#2360;&#2381;&#2357;&#2366;&#2327;&#2340; Ranchi - &#2350;&#2369;&#2326;&#2381;&#2351;&#2350;&#2306;&#2340;&#2381;&#2352;&#2368; &#2361;&#2375;&#2350;&#2306;&#2340; &#2360;&#2379;&#2352;&#2375;&#2344; &#2342;&#2381;&#2357;&#2366;&#2352;&#2366; &#2333;&#2366;&#2352;&#2326;&#2306;&#2337; &#2325;&#2375; 36 &#2323;&#2348;&#2368;&#2360;&#2368; &#2332;&#2366;&#2340;&#2367;&#2351;&#2379;&#2306;...</p>--%>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:DataList>


                        <asp:Repeater ID="rptPager" runat="server">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkPage" runat="server"
                                    Text='<%#Eval("Text") %>'
                                    CommandArgument='<%#Eval("Value") %>'
                                    Enabled='<%#Eval("Enabled") %>'
                                    OnClick="Page_Changed"
                                    ForeColor="#267CB2"
                                    Font-Bold="true" />
                            </ItemTemplate>
                        </asp:Repeater>


                        <%--</article>--%>
                        <%-- <article id="post-4240" class="latest-posts-list col-1 float-l pad archive-layout-list archive-image-left post-4240 post type-post status-publish format-standard has-post-thumbnail hentry category-news tag-corona tag-culture tag-media tag-news tag-sports">
                            <div class="archive-list-post list-style">
                                <div class="read-single color-pad">

                                    <div class="data-bg read-img pos-rel col-2 float-l read-bg-img af-sec-list-img"
                                        data-background="wp-content/uploads/2020/08/20200823_164904-720x475.jpg">
                                        <img src="wp-content/uploads/2020/08/20200823_164904-720x475.jpg">
                                        <div class="min-read-post-format">
                                            <span class="min-read-item">
                                                <span class="min-read">5 min read</span>                            </span>
                                        </div>

                                        <a href="2020/08/23/jharkhand-on-the-occasion-of-the-birth-anniversary-of-former-vice-chancellor-dr-ram-dayal-munda-his-statue-was-garlanded-and-planted-a-tree/Default.aspx"></a>
                                    </div>


                                    <div class="read-details col-2 float-l pad af-sec-list-txt color-tp-pad">
                                        <div class="read-categories">
                                            <ul class="cat-links">
                                                <li class="meta-category">
                                                    <a class="newsever-categories category-color-1" href="category/media-2/news/Default.aspx" alt="View all posts in NEWS">NEWS
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="read-title">
                                            <h4>
                                                <a href="2020/08/23/jharkhand-on-the-occasion-of-the-birth-anniversary-of-former-vice-chancellor-dr-ram-dayal-munda-his-statue-was-garlanded-and-planted-a-tree/Default.aspx">Jharkhand -माण्डर विधायक बंधु तिर्की ने डॉ रामदयाल मुंडा जी की जयंती के अवसर पर उनकी प्रतिमा पर माल्यार्पण कर वृक्षारोपण किया,</a>
                                            </h4>
                                        </div>
                                        <div class="entry-meta">

                                            <span class="author-links">

                                                <span class="item-metadata posts-date">
                                                    <i class="fa fa-clock-o"></i>
                                                    August 23, 2020            </span>

                                                <span class="item-metadata posts-author byline">
                                                    <i class="fa fa-pencil-square-o"></i>
                                                    <a href="author/indiamoves/Default.aspx">admin                        </a>
                                                </span>

                                            </span>
                                        </div>

                                        <div class="read-descprition full-item-discription">
                                            <div class="post-description">
                                                <p>&#2346;&#2370;&#2352;&#2381;&#2357; &#2325;&#2369;&#2354;&#2346;&#2340;&#2367; &#2337;&#2377; &#2352;&#2366;&#2350;&#2342;&#2351;&#2366;&#2354; &#2350;&#2369;&#2306;&#2337;&#2366; &#2332;&#2368; &#2325;&#2368; &#2332;&#2351;&#2306;&#2340;&#2368; &#2325;&#2375; &#2309;&#2357;&#2360;&#2352; &#2346;&#2352; &#2313;&#2344;&#2325;&#2368; &#2346;&#2381;&#2352;&#2340;&#2367;&#2350;&#2366; &#2346;&#2352; &#2350;&#2366;&#2354;&#2381;&#2351;&#2366;&#2352;&#2381;&#2346;&#2339; &#2325;&#2352; &#2357;&#2371;&#2325;&#2381;&#2359;&#2366;&#2352;&#2379;&#2346;&#2339; &#2325;&#2367;&#2351;&#2366; &#2327;&#2351;&#2366; JHARKHAND...</p>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </article>
                        --%>
                    </div>
                    <!--</div>-->
                </main><!-- #main -->
                <%-- <div class="col-1">
                    <div class="newsever-pagination">

                        <nav class="navigation pagination" role="navigation" aria-label="Posts">
                            <h2 class="screen-reader-text">Posts navigation</h2>
                            <div class="nav-links">
                                <span aria-current="page" class="page-numbers current">1</span>
                                <a class="page-numbers" href="page/2/Default.aspx">2</a>
                                <a class="page-numbers" href="page/3/Default.aspx">3</a>
                                <a class="page-numbers" href="page/4/Default.aspx">4</a>
                                <span class="page-numbers dots">&hellip;</span>
                                <a class="page-numbers" href="page/40/Default.aspx">40</a>
                                <a class="next page-numbers" href="page/2/Default.aspx">Next</a>
                            </div>
                        </nav>
                    </div>
                </div>--%>
            </div>
            <!-- #primary -->
            <div id="secondary" class="sidebar-area aft-sticky-sidebar">
                <div class="theiaStickySidebar">
                    <aside class="widget-area color-pad">
                        <div id="search-2" class="widget newsever-widget widget_search">
                            <form role="search" method="get" class="search-form" action="https://indiamoves.in/">
                                <label>
                                    <span class="screen-reader-text">Search for:</span>
                                    <input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s" />
                                </label>
                                <input type="submit" class="search-submit" value="Search" />
                            </form>
                        </div>
                        <div id="recent-posts-2" class="widget newsever-widget widget_recent_entries">
                            <h2 class="widget-title widget-title-1"><span class="header-after">Recent Posts</span></h2>
                            <asp:DataList ID="dlRecentPost" runat="server">
                                <ItemTemplate>
                                    <ul>
                                        <li>
                                            
                                            <a href="/ViewNews.aspx?id=<%#Eval("NewsId")%>" class="link-product-add-cart">
                                                        <asp:Label ID="labHeadLine" runat="server" Text='<%#Eval("name")%>' /></a>
                                        </li>
                                    </ul>
                                </ItemTemplate>
                            </asp:DataList>

                        </div>
                        <div id="recent-comments-2" class="widget newsever-widget widget_recent_comments">
                            <h2 class="widget-title widget-title-1"><span class="header-after">Recent Comments</span></h2>
                            <ul id="recentcomments">
                                <li class="recentcomments"><span class="comment-author-link">MZKhan</span> on <a href="2020/07/29/ranchi-pream-chand-ki-khaniya-facebook-live/Default.aspx#comment-138">Ranchi के फेसबुक पेज से प्रेमचंद की कहानी ठाकुर का कुआं का लाइव पाठ किया.</a></li>
                                <li class="recentcomments"><span class="comment-author-link">MZKhan</span> on <a href="2020/07/18/start-up-adivasi-online-platform-jharkhand/Default.aspx#comment-91">आदिवासियों उद्यमियों को बढ़ावा देता ,आनॅलाईन प्लेटफार्म</a></li>
                                <li class="recentcomments"><span class="comment-author-link"><a href='2020/07/21/jharkhand-news-pan-masala-and-ghutkha-banned-2021/Default.aspx' rel='external nofollow ugc' class='url'>jharkhand में गुटखा और पान मसाला 2021 तक बैन - इंडिया Moves</a></span> on <a href="2020/07/21/jharkhand-news-update-chamber-of-commerce/Default.aspx#comment-90">jharkhand news today   चेंबर ऑफ कॉमर्स ने लिया बड़ा फैसला</a></li>
                                <li class="recentcomments"><span class="comment-author-link"><a href='2020/07/21/jharkhand-news-update-chamber-of-commerce/Default.aspx' rel='external nofollow ugc' class='url'>jharkhand news today में चेंबर ऑफ कॉमर्स ने लिया बड़ा फैसला - इंडिया Moves</a></span> on <a href="2020/07/21/jharkhnad-news-update2020/Default.aspx#comment-89">JHARKHNAD NEWS मास्क अपनाये, कोरोना भगाये &#8220;</a></li>
                                <li class="recentcomments"><span class="comment-author-link"><a href='2020/07/06/jharkhand-update-4/Default.aspx' rel='external nofollow ugc' class='url'>महापौर श्रीमती आशा लकड़ा -रांची झारखंड &#8211; इंडिया Moves</a></span> on <a href="2020/07/06/bjp-deepak-prakash-jharkhand/Default.aspx#comment-46">राज्य की कानून व्यवस्था को लेकर हेमंत सरकार गंभीर नहीं -दीपक प्रकाश बीजेपी jharkhand</a></li>
                            </ul>
                        </div>
                        <div id="archives-2" class="widget newsever-widget widget_archive">
                            <h2 class="widget-title widget-title-1"><span class="header-after">Archives</span></h2>
                            <ul>
                                <li><a href='2020/08/Default.aspx'>August 2020</a></li>
                                <li><a href='2020/07/Default.aspx'>July 2020</a></li>
                                <li><a href='2020/06/Default.aspx'>June 2020</a></li>
                            </ul>

                        </div>
                        <div id="categories-2" class="widget newsever-widget widget_categories">
                            <h2 class="widget-title widget-title-1"><span class="header-after">Categories</span></h2>
                            <ul>
                                <li class="cat-item cat-item-237"><a href="category/breaking-news/Default.aspx">Breaking-news</a>
                                </li>
                                <li class="cat-item cat-item-227"><a href="category/health-fitness/corona/Default.aspx">CORONA</a>
                                </li>
                                <li class="cat-item cat-item-231"><a href="category/media-2/culture/Default.aspx">CULTURE</a>
                                </li>
                                <li class="cat-item cat-item-35"><a href="category/health-fitness/Default.aspx">Health &amp; Fitness</a>
                                </li>
                                <li class="cat-item cat-item-40"><a href="category/lifestyle/Default.aspx">Lifestyle</a>
                                </li>
                                <li class="cat-item cat-item-43"><a href="category/media-2/Default.aspx">Media</a>
                                </li>
                                <li class="cat-item cat-item-232"><a href="category/media-2/news/Default.aspx">NEWS</a>
                                </li>
                                <li class="cat-item cat-item-66"><a href="category/sport/Default.aspx">Sport</a>
                                </li>
                                <li class="cat-item cat-item-236"><a href="category/tech/Default.aspx">Tech</a>
                                </li>
                                <li class="cat-item cat-item-1"><a href="category/uncategorized/Default.aspx">Uncategorized</a>
                                </li>
                            </ul>

                        </div>
                        <div id="meta-2" class="widget newsever-widget widget_meta">
                            <h2 class="widget-title widget-title-1"><span class="header-after">Meta</span></h2>
                            <ul>
                                <li><a rel="nofollow" href="wp-login.html">Log in</a></li>
                                <li><a href="feed/Default.aspx">Entries feed</a></li>
                                <li><a href="comments/feed/Default.aspx">Comments feed</a></li>

                                <li><a href="https://indiamoves.in/">indiamoves.in</a></li>
                            </ul>

                        </div>
                    </aside>
                </div>
            </div>
        </section>


    </div>
</asp:Content>
