﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IndiaMoves.Models;

namespace IndiaMoves
{
    public partial class ViewNews : System.Web.UI.Page
    {
        
        IndiamovesDBEntities db = new IndiamovesDBEntities();
        public int nid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            nid = Int32.Parse(Request.QueryString["id"].ToString());
            getSingleNews(nid);
            getSingleNewsNameDesc(nid);
            getRecentpost();

            SplitAnswer(nid);
        }

        private void getSingleNews(int nwsId)
        {
            var a = db.NewsImages.Where(s => s.isCover == true && s.News.NewsId==nwsId).Select(m => new { newsId = m.NewsID, path = m.Path.Substring(1, m.Path.Length), name = m.News.NewsHeadline, UploadBy = m.News.UserMaster.UserID, UploadDate = m.News.UploadDate }).ToList();
            dlViewSingle.DataSource = a;
            dlViewSingle.DataBind();
        }

        private void getSingleNewsNameDesc(int nwsId)
        {
            string fshare = "https://www.facebook.com/sharer/sharer.php?u=https://www.indiamoves.world/ViewNews.aspx?id=" + nwsId;
            var a = db.NewsImages.Where(s => s.isCover == true && s.News.NewsId == nwsId).Select(m => new { name = m.News.NewsHeadline, UploadBy = m.News.UserMaster.UserID, Description = m.News.Description, fbUrl= fshare }).ToList();
            dlNewsBody.DataSource = a;
            dlNewsBody.DataBind();
        }
        private void getRecentpost()
        {
            //all news Recent Post

            DateTime today = DateTime.Now;
            DateTime last10 = today.AddDays(-10);
            var b = db.News.Where(s => s.UploadDate > last10).Select(m => new { newsId = m.NewsId, name = m.NewsHeadline, }).ToList();
            dlRecentPost.DataSource = b;
            dlRecentPost.DataBind();
        }

        private void SplitAnswer(int nId)
        {

            string an = "";

            var ns = db.News.Where(l => l.NewsId == nid).FirstOrDefault();
            an = ns.Description;
            int count = 0;
            foreach (char c in an)
            {
                if ('>' == c)
                    count++;
            }

            string[] FtAns = new string[count + 1];
            FtAns = an.Split('>');
            List<string> FtAnsw = new List<string>();
            FtAnsw = FtAns.ToList();



            BulletedList1.DataSource = FtAnsw;
            BulletedList1.DataBind();

            //Regex regex = new Regex("[\u0600-\u06ff]|[\u0750-\u077f]|[\ufb50-\ufc3f]|[\ufe70-\ufefc]");  // arabic character

            Regex regex = new Regex("/^[o]*$/i");//^wp.*php$

            int cnt = BulletedList1.Items.Count;
            for (int i = 0; i < cnt; i++)
                if (regex.IsMatch(BulletedList1.Items[i].Value))
                    BulletedList1.Items[i].Attributes["style"] = "color:green;font-size:22px;font-family:Arial;text-align:center";
        }
    }
}