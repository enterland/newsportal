﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IndiaMoves.Models;

namespace IndiaMoves.Admin
{
    
    public partial class Login : System.Web.UI.Page
    {
        IndiamovesDBEntities db = new IndiamovesDBEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                pnlError.Visible = false;
                
                
            }
            if (Session["adUser"] != null)
            {
                string uid = Session["adUser"].ToString();
                Response.Redirect("/Admin/AdminHome.aspx?uid=" + uid);
            }
        }

        protected void btnAdminLogin_Click(object sender, EventArgs e)
        {
            try
            {
                UserMaster u = db.UserMasters.Single(m => m.UserID == txtUserName.Text && m.Pass == txtPass.Text);

                Session["adUser"] = u.UserID;
                Response.Redirect("/Admin/AdminHome.aspx");
            }
            catch (Exception)
            {
                pnlError.Visible = true;
                
            }
        }
    }
}