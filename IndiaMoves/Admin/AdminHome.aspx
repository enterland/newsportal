﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="AdminHome.aspx.cs" Inherits="IndiaMoves.Admin.AdminHome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="text-align: center">
        <h1 style="color: white; background-color: #01BC8C; font-size: xx-large">News</h1>
    </div>

    <div>
        <asp:Label ID="labHading" runat="server" Font-Size="X-Large" ForeColor="#00cc99" />
        <asp:Label ID="labId" Visible="false" runat="server" />
    </div>
    <div></div>
    <br />
    <br />
    <div style="text-align: right">
        <asp:Button CssClass="btn btn-success" ID="btnAddNews" Text="Add New News" runat="server" OnClick="btnAddNews_Click"/>
    </div>
    <br />
    <br />
    <asp:Panel CssClass="card-panel gradient-45deg-red-pink gradient-shadow" ID="pnlError" runat="server" Visible="false">
        <a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>
        <asp:Label ID="labError" Text="" runat="server"></asp:Label>
        <%--News name already exists. Please rename and try again.--%>
    </asp:Panel>
    <div>
        <asp:Panel ID="pnlGrid" Visible="True" runat="server">
            <div class="panel panel-primary filterable">
                <div class="panel-body table-responsive">
                    <asp:GridView ID="gvNewsList" ClientIDMode="Static" CssClass="striped" ShowHeader="true"
                        runat="server" AutoGenerateColumns="false" Width="100%" OnRowCommand="gvNewsList_RowCommand">
                        <Columns>
                            <asp:BoundField DataField="NewsId" HeaderText="ID" Visible="false" />
                            <asp:BoundField DataField="Category" HeaderText="News Category Name" />
                            <asp:BoundField DataField="News" HeaderText="News Name" />
                            <%--<asp:BoundField DataField="Description" HeaderText="Qty" />--%>
                            <%--<asp:BoundField DataField="Image" HeaderText="News Image" />--%>
                            <asp:TemplateField HeaderText="Actions">
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" runat="server" CssClass="material-icons btn-floating btn-flat blue-text" Text="edit" CommandName="EditRow" CommandArgument='<%# Eval("NewsId") %>' />
                                    <asp:Button ID="btnDel" runat="server" CssClass="material-icons btn-floating btn-flat blue-text" Text="delete" CommandName="DeleteRow" CommandArgument='<%# Eval("NewsId") %>' />
                                    <%--<a data-id='<%# Eval("BoutNewsId") %>' class="material-icons btn-floating btn-flat blue-text" float: left;">
                                            <i class="material-icons btn-floating btn-flat blue-text" data-name="trash" data-loop="true" data-hovercolor="black" data-size="14">delete</i>
                                            
                                        </a>--%>
                                </ItemTemplate>

                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlAddEdit" Visible="false" runat="server">
            <div>
                <p>
                    <asp:Label CssClass="col-md-3 control-label" runat="server" ID="LabNewsId" Text="News Id :" Font-Bold="true" />
                    <asp:DropDownList Width="30%" CssClass="form-control select2" ID="ddlNews" Visible="false" runat="server" AutoPostBack="True" />
                </p>

                <p>
                    <asp:DropDownList Width="30%" CssClass="" ID="ddlNewsCategory" runat="server" />
                </p>

                <p>
                    <asp:Label CssClass="col-md-3 control-label" runat="server" ID="LabNewsName" Text="News Name :" Font-Bold="true" />
                    <asp:TextBox ID="txtNewsName" runat="server" CssClass="form-control" Width="30%" />
                    <asp:RequiredFieldValidator ID="rftxtNewsName" runat="server" ControlToValidate="txtNewsName" Display="Dynamic" ErrorMessage="Required!" ForeColor="Red" />
                </p>

                <p>
                    <asp:Label CssClass="col-md-3 control-label" runat="server" ID="Label1" Text="Upload Date :" Font-Bold="true" />
                    <asp:TextBox ID="txtUploadDate" runat="server" CssClass="form-control" Width="30%" DataFormatString="{0:dd/MM/yyyy}"/>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUploadDate" Text="" Display="Dynamic" ErrorMessage="Required!" ForeColor="Red" />
                </p>

                <p>
                    <asp:Label CssClass="col-md-3 control-label" runat="server" ID="LabNewsDesc" Text="Description :" Font-Bold="true" />
                    <asp:TextBox ID="txtNewsDesc" runat="server" CssClass="form-control" Width="70%" Height="50%" TextMode="MultiLine" />
                </p>
                <p>
                    <asp:Label CssClass="col-md-3 control-label" runat="server" ID="LabUploadImg" Text=" News Image Upload :" Font-Bold="true" />
                </p>
                <p>
                    <asp:FileUpload ID="fuNewsImage" Width="300px" runat="server" AllowMultiple="true" CssClass="form-control"></asp:FileUpload>
                </p>
                <br />
                <br />
                <div id="dvPreview">
                </div>
                <div class="form-group">
                    <asp:DataList ID="dlNewsImage" RepeatColumns="5" CellPadding="2" runat="server">
                        <ItemTemplate>
                            <asp:HiddenField ID="hidBoutImageId" runat="server" Value='<%# Eval("NewsID") %>' />
                            <div class="">
                                <asp:Image ID="Image1" runat="server" Width="100px" Height="120px" ImageUrl='<%# Eval("Path") %>' />
                            </div>
                            <div>
                                is Cover?
                                <a id="btnActive" runat="server" data-id='<%# Eval("NewsID") %>' class="btn-floating green btn-flat white-text package-active" visible='<%# Convert.ToBoolean(Eval("isCover"))==true?true:false %>'>
                                    <i class="material-icons" data-name="done" data-loop="true" data-color="#fb772f" data-hovercolor="black" data-size="20">done</i>
                                </a>
                                <a id="btnDeActive" runat="server" data-id='<%# Eval("NewsID") %>' class="btn-floating waves-effect waves-light red accent-2 package-deactive" visible='<%# Convert.ToBoolean(Eval("isCover"))==false?true:false %>'>
                                    <i class="material-icons" data-name="clear" data-loop="true" data-color="#EF6F6C" data-hovercolor="black" data-size="20">clear</i>
                                </a>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
                <p>
                    <asp:Button CssClass="btn btn-success" ID="btnSubmitNews" Text="Submit" runat="server" OnClick="btnSubmitNews_Click" />&nbsp;&nbsp;&nbsp;&nbsp; 
                        <asp:Button CssClass="btn btn-danger" ID="buttonCancel" Text="Cancel" runat="server" OnClick="buttonCancel_Click" />
                    <asp:Button ID="btnRest" CssClass="btn btn-success" runat="server" Text="Reset" OnClick="btnRest_Click" />
                </p>
            </div>
        </asp:Panel>
    </div>
    <script language="javascript" type="text/javascript">
        window.onload = function () {
            var fileUpload = document.getElementById("ContentPlaceHolder1_fuNewsImage");
            fileUpload.onchange = function () {
                if (typeof (FileReader) != "undefined") {
                    var dvPreview = document.getElementById("dvPreview");
                    dvPreview.innerHTML = "";
                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                    for (var i = 0; i < fileUpload.files.length; i++) {
                        var file = fileUpload.files[i];
                        if (regex.test(file.name.toLowerCase())) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var img = document.createElement("IMG");
                                img.height = "100";
                                img.width = "100";
                                img.src = e.target.result;
                                dvPreview.appendChild(img);
                            }
                            reader.readAsDataURL(file);
                        } else {
                            alert(file.name + " is not a valid image file.");
                            dvPreview.innerHTML = "";
                            return false;
                        }
                    }
                } else {
                    alert("This browser does not support HTML5 FileReader.");
                }
            }
        };
    </script>
</asp:Content>
