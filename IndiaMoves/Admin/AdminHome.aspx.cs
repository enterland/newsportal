﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntityFramework.Extensions;
using IndiaMoves.Models;

namespace IndiaMoves.Admin
{
    public partial class AdminHome : System.Web.UI.Page
    {

        IndiamovesDBEntities db = new IndiamovesDBEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //txtUploadDate.Text = DateTime.Now.ToString("yyy-MM-dd");
                if (Session["adUser"] == null)
                {
                    Response.Redirect("/Admin/Login.aspx");
                }
                if (!IsPostBack)
                {
                    getGrid();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        private void getGrid()
        {
            var a = db.News.Select(m => new { Category = m.NewsCategoryMaster.CategoryName, News = m.NewsHeadline, NewsId = m.NewsId }).ToList();

            gvNewsList.DataSource = a;
            gvNewsList.DataBind();
        }

        private void getddlnewsCat()
        {

            var a = db.NewsCategoryMasters.ToList().OrderBy(m => m.CategoryName);

            ddlNewsCategory.DataSource = a;
            ddlNewsCategory.DataValueField = "newsCatId";
            ddlNewsCategory.DataTextField = "CategoryName";
            ddlNewsCategory.DataBind();
            ddlNewsCategory.Items.Insert(0, "Select News Category");

            ddlNewsCategory.Visible = true;

        }

        private void getddlId()
        {
            var a = db.News.ToList().OrderBy(m => m.NewsHeadline);
            ddlNews.DataSource = a;
            ddlNews.DataValueField = "NewsId";
            ddlNews.DataTextField = "NewsId";
            ddlNews.DataBind();

            ddlNews.Visible = false;

        }

        private void GetNewsImage(int NewsId)
        {
            dlNewsImage.DataSource = db.NewsImages.Where(hi => hi.NewsID == NewsId)
                .Select(m => new { NewsID = m.NewsID, Path = m.Path, isCover = m.isCover }).ToList();
            dlNewsImage.DataBind();
        }
        protected void gvNewsList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            pnlError.Visible = false;
            labHading.Text = "Edit News";
            getddlnewsCat();
            ddlNews.Enabled = true;

            ddlNews.Enabled = true;


            int id = Convert.ToInt32(e.CommandArgument);
            getddlId();
            ddlNews.SelectedValue = id.ToString();

            News t = db.News.Single(s => s.NewsId == id);

            ddlNewsCategory.SelectedValue = t.NewsCatID.ToString();
            txtNewsName.Text = t.NewsHeadline;
            txtUploadDate.Text = Convert.ToDateTime(t.UploadDate).ToShortDateString();
            txtNewsDesc.Text = t.Description;
            labId.Text = t.NewsId.ToString();

            GetNewsImage(id);


            switch (e.CommandName)
            {
                case "EditRow":
                    getddlId();
                    pnlAddEdit.Visible = true;
                    pnlGrid.Visible = false;
                    btnAddNews.Visible = false;
                    LabNewsId.Visible = false;
                    break;
                case "DeleteRow":
                    deleteProduct(id);
                    break;
                default:
                    break;
            }
        }

        protected void btnSubmitNews_Click(object sender, EventArgs e)
        {
            try
            {
                pnlError.Visible = false;
                if (labHading.Text == "Add News")
                {

                    if (db.News.Where(m => m.NewsHeadline == txtNewsName.Text).FirstOrDefault() != null)
                    {
                        pnlError.Visible = true;
                        labError.Text = "News name already exists. Please rename and try again.";
                    }
                    else
                    {
                        string updId = Session["adUser"].ToString();
                        UserMaster u = db.UserMasters.Single(m => m.UserID == updId);

                        int usrID = u.UID;

                        News t = new News
                        {
                            NewsCatID = Int32.Parse(ddlNewsCategory.SelectedValue),
                            Description = txtNewsDesc.Text,
                            NewsHeadline = txtNewsName.Text,
                            UploadDate = Convert.ToDateTime(txtUploadDate.Text),
                            PublishDate = Convert.ToDateTime(DateTime.Now.ToShortDateString()),
                            UploadUser = usrID
                        };
                        db.News.Add(t);

                        InsertNewsImage(t.NewsId);
                        setCover(t.NewsId);
                        db.SaveChanges();
                    }




                }
                else
                {
                    Edit();
                }
                if (pnlError.Visible == false)
                {
                    pnlGrid.Visible = true;
                    pnlAddEdit.Visible = false;
                    pnlError.Visible = false;
                    btnAddNews.Visible = true;
                    labHading.Text = "";

                    getGrid();
                }
            }
            catch (Exception ex)
            {
                pnlError.Visible = true;
                labError.Text = ex.Message;
            }
        }
        private void InsertNewsImage(int NewsId)
        {
            try
            {
                pnlError.Visible = false;
                if (fuNewsImage.HasFiles)
                {
                    foreach (HttpPostedFile uploadedFile in fuNewsImage.PostedFiles)
                    {
                        uploadedFile.SaveAs(System.IO.Path.Combine(Server.MapPath("~/NewsImage/"), uploadedFile.FileName));
                        NewsImage ni = new NewsImage();
                        ni.Path = "~/NewsImage/" + uploadedFile.FileName;
                        ni.NewsID = NewsId;
                        ni.NewsImageName = uploadedFile.FileName;
                        ni.isCover = false;
                        db.NewsImages.Add(ni);
                        //db.SaveChanges();
                    }
                }
                db.SaveChanges();

            }
            catch (Exception ex)
            {

                pnlError.Visible = true;
                labError.Text = ex.Message;
            }
        }

        private void setCover(int NewsId)
        {
            var bi = db.NewsImages.Where(m => m.NewsID == NewsId);
            foreach (NewsImage b in bi)
            {
                b.isCover = true;
                break;
            }

        }
        private void Edit()
        {
            int a = Int32.Parse(ddlNews.SelectedValue);


            News t = db.News.Single(m => m.NewsId == a);
            t.NewsCatID = Int32.Parse(ddlNewsCategory.SelectedValue);

            t.NewsHeadline = txtNewsName.Text;
            t.UploadDate = Convert.ToDateTime(txtUploadDate.Text);
            t.Description = txtNewsDesc.Text;
            InsertNewsImage(t.NewsId);
            db.SaveChanges();
        }
        protected void buttonCancel_Click(object sender, EventArgs e)
        {
            pnlError.Visible = false;
            pnlGrid.Visible = true;
            pnlAddEdit.Visible = false;
            btnAddNews.Visible = true;
            labHading.Text = "";
            getGrid();
        }

        protected void btnAddNews_Click(object sender, EventArgs e)
        {
            labHading.Text = "Add News";
            getddlnewsCat();
            LabNewsId.Visible = false;
            ddlNews.Enabled = false;
            txtUploadDate.Text = Convert.ToDateTime(DateTime.Now).ToShortDateString();
            if (pnlAddEdit.Visible)
            {
                txtNewsName.Text = "";
                ddlNews.SelectedIndex = -1;

                //ddlNews.SelectedIndex = -1;

            }
            else
            {
                pnlAddEdit.Visible = true;
                pnlGrid.Visible = false;
                btnAddNews.Visible = false;
                txtNewsName.Text = "";
                ddlNews.SelectedIndex = -1;

                ddlNews.SelectedIndex = -1;
            }
        }

        private void deleteProduct(int id)
        {
            try
            {



                var imgs = from m in db.NewsImages where m.NewsID == id select m;
                string imName = "";
                foreach (NewsImage b in imgs)
                {
                    imName = b.NewsImageName;
                    var query = from o in Directory.GetFiles(System.IO.Path.Combine(Server.MapPath("~/NewsImage/")), "*.*",
                    SearchOption.AllDirectories)
                                let x = new FileInfo(o)
                                where x.Name == imName
                                select o;
                    foreach (var item in query)
                    {
                        File.Delete(item);
                    }
                }



                db.NewsImages.Where(m => m.NewsID == id).Delete();
                db.SaveChanges();

                db.News.Where(s => s.NewsId == id).Delete();
                db.SaveChanges();
                getGrid();


            }
            catch (Exception ex)
            {

                pnlError.Visible = true;
                labError.Text = ex.Message;
            }


        }

        protected void btnRest_Click(object sender, EventArgs e)
        {
            ddlNewsCategory.SelectedIndex = -1;
            txtNewsName.Text = "";
            txtUploadDate.Text = "";
            txtNewsDesc.Text = "";

            fuNewsImage = new FileUpload();
        }
    }
}