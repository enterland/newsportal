﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IndiaMoves.Models;

namespace IndiaMoves
{
    public partial class MasterStatic : System.Web.UI.MasterPage
    {
        IndiamovesDBEntities db = new IndiamovesDBEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            labHEadDate.Text = DateTime.Now.ToString("dd-MMMM-yyy");
            getExclusive();
            missedNews();
        }

        public void getExclusive()
        {
            DateTime todaym = DateTime.Now;
            DateTime last7m = todaym.AddDays(-7);
            var a = db.NewsImages.Where(s => s.isCover == true && s.News.UploadDate>last7m).Select(m => new { newsId = m.NewsID, path = m.Path.Substring(1, m.Path.Length), name = m.News.NewsHeadline, UploadDate = m.News.UploadDate, UploadBy = m.News.UserMaster.UserID, ShortDesc = (m.News.Description.Substring(0, 100) + " ....") }).OrderByDescending(k => k.newsId).ToList();
            dlExclusive.DataSource = a;
            dlExclusive.DataBind();
        }

        public void missedNews()
        {
            //all news Missed before 30days
            DateTime today = DateTime.Now;
            DateTime before30 = today.AddDays(-30);
            var x = db.NewsImages.Where(s => s.isCover == true && s.News.UploadDate < before30).Select(m => new { newsId = m.NewsID, path = m.Path.Substring(1, m.Path.Length), name = m.News.NewsHeadline, UploadDate = m.News.UploadDate, UploadBy = m.News.UserMaster.UserID, ShortDesc = (m.News.Description.Substring(0, 100) + " ....") }).OrderByDescending(k => k.newsId).Take(5).ToList();
            dlMissedNews.DataSource = x;
            dlMissedNews.DataBind();
        }
    }
}